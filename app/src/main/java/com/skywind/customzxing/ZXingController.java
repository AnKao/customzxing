package com.skywind.customzxing;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * Created by AlbertKao on 2017/7/25.
 */

public class ZXingController {

    private static ZXingController mZXingController;

    private IntentIntegrator integrator;

    public static ZXingController getInstance() {
        if (mZXingController == null) {
            mZXingController = new ZXingController();
        }
        return mZXingController;
    }

    /**
     * init before start scanning
     *
     * @param activity
     */
    public void init(Activity activity) {
        integrator = new IntentIntegrator(activity);
        integrator.setCaptureActivity(PortraitCaptureActivity.class);
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(false);
    }

    public void startScan() {
        if (integrator == null) {
            Log.e(getClass().getName(), "Please call \"init\" method before start scanning");
            return;
        }
        integrator.initiateScan();
    }

    /**
     * handle Activity result from ZXing
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * @return scanning result
     */
    public String parseActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result.getContents() != null) {
            return result.getContents().toString();
        } else {
            return "";
        }
    }

    /**
     * set camera type with camera id
     *
     * @param cameraId
     */
    public void setCameraId(int cameraId) {
        integrator.setCameraId(cameraId);
    }

    /**
     * set camera timeout with millisecond
     *
     * @param time
     */
    public void setTimeout(long time) {
        integrator.setTimeout(time);
    }

    /**
     * default orientation locked, and is portrait
     * if you want to go to change the orientation,
     * please modify PortraitCaptureActivity in the manifest
     *
     * @param lock
     */
    public void setOrientationLocked(boolean lock) {
        integrator.setOrientationLocked(lock);
    }
}
