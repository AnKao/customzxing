package com.skywind.customzxing;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

public class PortraitCaptureActivity extends AppCompatActivity {

    private LinearLayout mLlTitleBarLeft;
    private ImageView mIvTitleBarLeft;

    private CaptureManager mCapture;
    private DecoratedBarcodeView mBarcodeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portrait_capture);
        initView();
        initTitleBar();

        mCapture = new CaptureManager(this, mBarcodeView);
        mCapture.initializeFromIntent(getIntent(), savedInstanceState);
        mCapture.decode();
        mBarcodeView.getViewFinder().setVisibility(View.INVISIBLE);
        checkCameraPermission();
    }

    private void initView() {
        mBarcodeView = (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
        mLlTitleBarLeft = (LinearLayout) findViewById(R.id.ll_title_bar_left_btn);
        mIvTitleBarLeft = (ImageView) findViewById(R.id.iv_title_bar_left_btn);
    }

    private void initTitleBar() {
        mIvTitleBarLeft.setImageResource(R.drawable.btn_close_w);
        mLlTitleBarLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBarcodeView.resume();
        mCapture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBarcodeView.pause();
        mCapture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCapture.onDestroy();
        mBarcodeView.destroyDrawingCache();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mCapture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return mBarcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        mCapture.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void checkCameraPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    CaptureManager.getCameraPermissionReqCode());
        } else {
            mCapture.onResume();
        }
    }

}
